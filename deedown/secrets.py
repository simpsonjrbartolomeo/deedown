import os

# Replace the default filler text with the actual IDs from your Deezer Developer account.
os.environ.setdefault('DEEZER_CLIENT_ID', 'clientid') # Deezer Application ID
os.environ.setdefault('DEEZER_CLIENT_SECRET', 'clientecret') # Deezer Secret Key
os.environ.setdefault('DEEZER_USER_ID', 'userid') # Deezer User ID from profile link

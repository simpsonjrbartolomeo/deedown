# deedown Documentation

This folder includes all of the docs for deedown. More can be found on the [wiki](https://gitlab.com/willtheorangeguy/deedown/-/wikis/home).

```text
├── docs
|   ├── images
|   ├── legal
|   |   ├── DEEZER.md
|   |   ├── PRIVACY.md
|   |   ├── SPOTIFY.md
|   |   ├── TERMS.md
|   ├── USAGE.md
|   ├── REQUIREMENTS.md
└── README.md
```
